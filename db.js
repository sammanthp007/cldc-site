const jsonIO = require('jsonfile');

var data = require('./app/src/db/db');
var path = './app/src/db/db.json';

/**
 * Object options, a new operator/post/legend/service. Fill in the fields for
 *         or more.
 */
var newOperator = {
    'name': '',
    'bio': '',
    'image': '',
};
var newService = {
    'name': '',
    'description': ''
};
var newLegend = {
    'name': '',
    'bio': '',
    'image', '',
};
var newUpdate = {
    'title': '',
    'content': ''
};


/**
 * Write options, pick from this list of possible write options and paste into
 *         code.
 */

/**
 * data['operators'].push(newOperator);
 * data['legends'].push(newLegend);
 * data['services'].push(newService);
 * data['updates'].push(newUpdate);
 */

/**
 * Finally write the file.
 */
jsonIO.writeFileSync(path, data);



/**
 * In the future, use readline to parse information from a text file or csv?
 * readline = require('readline')
 * rl = readline.createInterface({input: , output: })
 */
