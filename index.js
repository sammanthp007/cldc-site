// Modules
const bs = require('browser-sync');
const express = require('express');
const jade = require('jade');
const morgan = require('morgan');

const app = express();
const env = process.argv[2] || 'prod';
const port = (env != 'prod') ? 2368 : 3000;
const viewPath = (env != 'prod') ? '/app/src/jade' : '/app/dist/views';

/**
 * Express setup, compiling jade > html on request, fetching appropriate
 *         styles and serving our static assets.
 */
app.set('views', __dirname + viewPath);
app.set('view engine', 'jade');
app.use(morgan('combined'));

// Static server for all files
app.use(express.static(__dirname + '/app/dist'));

// Basic entry point route handled by server, all others are rendered client-side
app.get('*', function(req, res) {
    res.render('index');
});

// We'll refresh the page on any change to the client code.
app.listen(port, function() {
    if (env != 'prod') {
        bs({
            files: ['app/src/*/*.{jade,styl,json}'],
            open: false,
            proxy: 'localhost:' + port
        });
        console.log('Listening on port 2368 w/ browser-sync');
    } else {
        console.log('Listening on port ' + port);
    }
});

