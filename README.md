# CLDC Website
The main site for Computer Learning and Design Center at Howard University!

## Contents:
* [Getting Started](https://bitbucket.org/cldc-ops/cldc-site/overview#markdown-header-getting-started)
* [Modifying](https://bitbucket.org/cldc-ops/cldc-site/overview#markdown-header-modifying)
   * [Jade](https://bitbucket.org/cldc-ops/cldc-site/overview#markdown-header-pages)
   * [Styling](https://bitbucket.org/cldc-ops/cldc-site/overview#markdown-header-styling)
* [Road map](https://bitbucket.org/cldc-ops/cldc-site/overview#markdown-header-road-map)


## Getting Started
We've created the site using the following technologies:
* [React](https://facebook.github.io/react)
* [NodeJS](https://nodejs.org)
* [NPM](https://npmjs.org)
* [Stylus](https://learnboost.github.io/stylus)
* [Jade](http://jade-lang.com)
* [Babel](https://babeljs.io)

To get started, clone the repo:

```bash
git clone https://bitbucket.org/cldc-ops/cldc-site.git
```

Once inside the repo install all dependencies

```bash
npm install
```

We've also included browser-sync and nodemon to allow for server refreshes on file changes!

For started you should install nodemon globally so you can use it with any project:

```bash
npm install nodemon -g
```

You may need to run as admin/sudo to install properly.

Once that's finished you simple run nodemon with the 'e' flag to specify which files to watch  and you're good to go:

```bash
nodemon index.js
```
*Note: this method will refresh the browser on file changes but if you modify index.js node itself will refresh causing a new browser-sync instance*

Now you're ready to go!

## Modifying
Let's understand the structure of the site pages first.

```bash
app
├── dist
│   ├── assets
│   │   └── img
│   ├── css
│   ├── js
│   │   └── react-components
│   └── views
└── src
    ├── jade
    └── stylus
```

### Pages
So the source files are created using jade in src/jade. Upon delpoyment they are copied to and served from dist/views (think the public/ folder you usually see). For now once you finish editing the or adding a page, simple run:

```bash
npm run j2v
```

to copy the files over and you should see your changes on the server!

Another option would be to pass the 'dev' flag when running node to let the server know to directly use the src folder:

```bash
nodemon -e js,jade,styl index.js dev
```

### Styling
Changing the styling of the page will work in a similar way to jade, with one distinct difference. Since the stylus is generated for the jade pages it is read directly from the src files before being converted to plain css, meaning you can directly edit src/styl without any additional steps.

## Road Map
The site is pretty plain right now but we're planning some cool additions such as:
* Forms - Letting users make requests for software, services, the works
* Reservations - Letting instructors request lab space for a class using an interactive calendar
* Hosting - A portal to all the current projects being worked on by our operators.

### Feel free to check it out and submit a pull request
